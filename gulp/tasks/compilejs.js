/*compilejs.js
Processing library that moves js from resources to the web dist folder and minifies it
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

const gulp = require('gulp');
const uglify = require('gulp-uglify');

gulp.task('compilejs',['delete'], function () {
  gulp
    .src('./app/Resources/js/**/*')
    .pipe(uglify())
    .pipe(gulp.dest('./web/dist/js/'));

  return true;
});
