# Gulp configuration

To run, you will need to setup the following in the project root. It is assumed you have node and npm installed:

    npm install -S gulp
    mkdir gulptasks
    ln -s vendor/cyint/symphony-gulp-default/gulpfile.js ./
    ln -s vendor/cyint/symphony-gulp-default/gulp ./

In the gulptasks folder, setup local tasks. You need to define compilelibs.js that specifies which libraries to move over to dist.

To execute:

    ./node_modules/.bin/gulp
